const inventory = require('./cars.cjs');

function carsOfBMWAndAudi(inventoryData){
    const cars=[];
    if(inventoryData === undefined || inventoryData.length==0 || typeof inventoryData !== 'object' || !Array.isArray(inventoryData)){
        return cars;
    }
    for(let index=0;index<inventoryData.length;index++){
        if(inventoryData[index].car_make ==='BMW' || inventoryData[index].car_make === 'Audi'){
            cars.push(inventoryData[index]);
        }
    }
    return cars;
}
let cars = carsOfBMWAndAudi(inventory);
console.log(cars);
const JSONObj = JSON.stringify(cars);
let BMWAndAudi = JSONObj;
// console.log(BMWAndAudi);

module.exports = carsOfBMWAndAudi;