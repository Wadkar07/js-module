// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of: 
// "Last car is a *car make goes here* *car model goes here*"

const inventory = require('./cars.cjs');

function infoAbountLastCar(inventoryData){
    let car=[];
    if(inventoryData === undefined || inventoryData.length==0 || typeof inventoryData !== 'object' || !Array.isArray(inventoryData)){
        return car;
    }
    let last=inventoryData.length-1;
    car = inventoryData[last];
    return car;
}

const result = infoAbountLastCar(inventory);
console.log(`Last car is a ${result.car_make} ${result.car_model}`);

module.exports = infoAbountLastCar;