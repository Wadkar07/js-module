// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.

const inventory = require('./cars.cjs');

function getAllYears(inventoryData){
    let years=[];
    if(inventoryData === undefined || inventoryData.length==0 || typeof inventoryData !== 'object' || !Array.isArray(inventoryData)){
        return years;
    }
    for(let index=0;index<inventoryData.length;index++){
        years.push(inventoryData[index].car_year);
    }
    return years;
}

console.log(getAllYears(inventory));

module.exports = getAllYears;