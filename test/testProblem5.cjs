const expect = require('chai').expect;
const inventory = require('../cars.cjs');
const getAllYears = require('../problem4.cjs');
const allYears = getAllYears(inventory);
const problem5 = require('../problem5.cjs')

describe('Problem 5',()=>{
    it('returns how many cars are older than the year 2000',()=>{
        expect(problem5(allYears,2000)).to.eql([
            1983, 1990, 1995, 1987, 1996,
            1997, 1999, 1987, 1995, 1994,
            1985, 1997, 1992, 1993, 1964,
            1999, 1991, 1997, 1992, 1998,
            1965, 1996, 1995, 1996, 1999
          ]);
    });
    it('Returns empty array',()=>{
        expect(problem5()).to.eql([]);
        expect(problem5("allYears",2000)).to.eql([]);
        expect(problem5("allYears")).to.eql([]);
        expect(problem5(allYears,"2000")).to.eql([]);
        expect(problem5("2000")).to.eql([]);
        expect(problem5(allYears)).to.eql([]);
        expect(problem5(2000)).to.eql([]);
    });
});