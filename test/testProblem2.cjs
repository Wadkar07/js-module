const inventory = require('../cars.cjs')
const expect = require('chai').expect;
const problem2 = require('../problem2.cjs')

describe('Problem 2',() =>{
    it('returns last car from inventory',()=>{
        expect(problem2(inventory)).to.eql({ id: 50, car_make: 'Lincoln', car_model: 'Town Car', car_year: 1999 });
    });
    it('Returns empty array',()=>{
        expect(problem2()).to.eql([]);
        expect(problem2("inventory")).to.eql([]);
    })
});