var expect = require('chai').expect;
const inventory = require('../cars.cjs');
const problem1 = require('../problem1.cjs')

describe('Problem 1',() => {
    it('Return the information for a car with an id of 33 on his lot',()=>{
        expect(problem1(inventory,33)).to.eql({id: 33, car_make: 'Jeep', car_model: 'Wrangler', car_year: 2011});
    })
    it('Returns empty array',()=>{
        expect(problem1()).to.eql([]);
        expect(problem1(inventory)).to.eql([]);
        expect(problem1(2)).to.eql([]);
        expect(problem1([],3)).to.eql([]);
        expect(problem1({id: 33, name: "Test", length: 10}, 33)).to.eql([]);
        expect(problem1(inventory,[])).to.eql([]);
    })
});