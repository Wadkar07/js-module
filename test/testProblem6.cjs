const { expect } = require('chai');
const inventory = require('../cars.cjs');
const problem6 = require('../problem6.cjs')

describe('Problem 6',()=>{
    it('returns only BMW and Audi cars within the inventory',()=>{
        expect(problem6(inventory)).to.eql([
            { id: 6, car_make: 'Audi', car_model: 'riolet', car_year: 1995 },
            {
              id: 8,
              car_make: 'Audi',
              car_model: '4000CS Quattro',
              car_year: 1987
            },
            { id: 25, car_make: 'BMW', car_model: '525', car_year: 2005 },
            { id: 30, car_make: 'BMW', car_model: '6 Series', car_year: 2010 },
            { id: 44, car_make: 'Audi', car_model: 'Q7', car_year: 2012 },
            { id: 45, car_make: 'Audi', car_model: 'TT', car_year: 2008 }
          ])
    });
    it('Returns empty array',()=>{
        expect(problem6()).to.eql([]);
        expect(problem6("inventory")).to.eql([]);
    })
});