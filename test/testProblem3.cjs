const expect = require('chai').expect;
const inventory = require('../cars.cjs');
const problem3 = require('../problem3.cjs');

describe('Problem 3',() =>{
    it('returns all car models in alphabetical order',()=>{
        expect(problem3(inventory)).to.eql(['300M','4000CS Quattro','525','6 Series','Accord','Aerio','Bravada','Camry','Cavalier','Ciera','Defender Ice Edition','E-Class','Econoline E250','Escalade','Escort','Esprit','Evora','Express 1500','Familia','Fortwo','G35','Galant','GTO','Intrepid','Jetta','LSS','Magnum','Miata MX-5','Montero Sport','MR2','Mustang','Navigator','Prizm','Q','Q7','R-Class','Ram Van 1500','Ram Van 3500','riolet','Sebring','Skylark','Talon','Topaz','Town Car','TT','Windstar','Wrangler','Wrangler','XC70','Yukon']);
    })
    it('Returns empty array',()=>{
        expect(problem3()).to.eql([]);
        expect(problem3("inventory")).to.eql([]);
    })
})