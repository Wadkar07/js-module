// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

const inventory = require('./cars.cjs');
const getAllYears = require('./problem4.cjs');
const allYears = getAllYears(inventory);

function carsMAdeBefore2000(years,year){
    let olderCars=[];
    if(years=== undefined || year === undefined || typeof years!== 'object' ||typeof year !== 'number'||!Array.isArray(years)||years.length === 0) {
        return olderCars;
    }
    for(let index=0;index<years.length;index++){
        if(years[index]<year){
            olderCars.push(years[index]);
        }
    }
    return olderCars;
}
let year=2000;
const olderCarsYears = carsMAdeBefore2000(allYears,year);
const olderCarsCount = olderCarsYears.length;

console.log(`There are ${olderCarsCount} cars that are older than year ${year}`);

module.exports = carsMAdeBefore2000;