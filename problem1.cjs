// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of: 
// "Car 33 is a *car year goes here* *car make goes here* *car model goes here*"

const inventory = require('./cars.cjs')
function getCarWithId(inventoryData,id){
    let car =[];
    
    if(inventoryData === undefined || id === undefined || typeof inventoryData !== 'object' ||typeof id !== 'number'||!Array.isArray(inventoryData)||inventoryData.length === 0) {
        return car;  
    }
    for(let index=0;index<inventoryData.length;index++){
        if(inventoryData[index].id===id)
        car=inventoryData[index];
    }
    return car;
}
let id = 33;
const result = getCarWithId(inventory,id);
if(!Array.isArray(result)){
    console.log(`Car ${result.id} is a ${result.car_year} ${result.car_make} ${result.car_model}`);
}
else
console.log(result);

module.exports = getCarWithId;