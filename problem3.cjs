// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

const inventory = require('./cars.cjs');

function sortedModels(inventoryData){
    let models=[]
    if(inventoryData === undefined || inventoryData.length==0 || typeof inventoryData !== 'object' || !Array.isArray(inventoryData)){
        return models;
    }
    for(let index=0;index<inventoryData.length;index++){
        models.push(inventory[index].car_model)
    }
    models.sort((a,b)=>a.localeCompare(b,undefined,{sensitivity: 'base'}));
    return models;
}

const carModels = sortedModels(inventory);
if(carModels.length >0)
for(let index=0;index<carModels.length;index++){
    console.log(carModels[index]);
}
else{
    console.log(carModels);
}
module.exports = sortedModels;
